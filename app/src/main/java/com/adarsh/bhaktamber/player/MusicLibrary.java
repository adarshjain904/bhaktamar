/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.adarsh.bhaktamber.player;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaMetadataCompat;


import com.adarsh.bhaktamber.BuildConfig;
import com.adarsh.bhaktamber.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

class MusicLibrary {

    private static final TreeMap<String, MediaMetadataCompat> music = new TreeMap<>();
    private static final HashMap<String, Integer> albumRes = new HashMap<>();
    private static final HashMap<String, Integer> musicRes = new HashMap<>();

    static {
        createMediaMetadataCompat(
                "bhaktamar_1",
                "श्री भक्तामर (संस्कृत)","अनुराधा पौडवाल","श्री भक्तामर","भक्तामर",
                108,
                R.raw.bhaktamar_anuradha_paudwal, R.drawable.ic_logo, "bhaktamar_anuradha_paudwal");
        createMediaMetadataCompat(
                "bhaktamar_2",
                "श्री भक्तामर (संस्कृत)", "रवीन्द्र जैन", "श्री भक्तामर", "भक्तामर", 108,
                R.raw.bhaktamar_saskrit_ravindranath, R.drawable.ic_logo, "bhaktamar_saskrit_ravindranath");
        createMediaMetadataCompat(
                "bhaktamar_3",
                "श्री भक्तामर (संस्कृत)", "लता मंगेशकर", "श्री भक्तामर", "भक्तामर", 108,
                R.raw.bhaktamar_saskrit_lata, R.drawable.ic_logo, "bhaktamar_saskrit_lata");
        createMediaMetadataCompat(
                "bhaktamar_4",
                "श्री भक्तामर (हिंदी)", "रवीन्द्र जैन", "श्री भक्तामर", "भक्तामर", 108,
                R.raw.bhaktamar_hindi_ravindranath, R.drawable.ic_logo, "bhaktamar_hindi_ravindranath");
        createMediaMetadataCompat(
                "bhaktamar_5",
                "श्री भक्तामर (हिंदी)", "पं. कमलकुमार शास्त्री", "श्री भक्तामर", "भक्तामर", 108,
                R.raw.bkatamar_hindi, R.drawable.ic_logo, "bhaktamar_hindi");
        createMediaMetadataCompat(
                "bhaktamar_6",
                "श्री भक्तामर (संस्कृत)", "आचार्य श्री मानतुंग", "श्री भक्तामर", "भक्तामर", 108,
                R.raw.bhaktamar_saskrit_1, R.drawable.ic_logo, "bhaktamar_saskrit_1");
        createMediaMetadataCompat(
                "bhaktamar_7",
                "श्री भक्तामर (संस्कृत)", "आचार्य श्री मानतुंग", "श्री भक्तामर", "भक्तामर", 108,
                R.raw.bhaktamar_saskrit_2, R.drawable.ic_logo, "bhaktamar_saskrit_2");
        createMediaMetadataCompat(
                "bhaktamar_8",
                "श्री भक्तामर (संस्कृत)", "आचार्य श्री मानतुंग", "श्री भक्तामर", "भक्तामर", 108,
                R.raw.bhaktamar_saskrit_3, R.drawable.ic_logo, "bhaktamar_saskrit_3");
    }

    public static String getRoot() {
        return "root";
    }

    public static String getSongUri(String mediaId) {
        return "android.resource://" + BuildConfig.APPLICATION_ID + "/" + getMusicRes(mediaId);
    }

    private static String getAlbumArtUri(String albumArtResName) {
        return "android.resource://" + BuildConfig.APPLICATION_ID + "/drawable/" + albumArtResName;
    }

    private static int getMusicRes(String mediaId) {
        return musicRes.containsKey(mediaId) ? musicRes.get(mediaId) : 0;
    }

    private static int getAlbumRes(String mediaId) {
        return albumRes.containsKey(mediaId) ? albumRes.get(mediaId) : 0;
    }

    public static Bitmap getAlbumBitmap(Context ctx, String mediaId) {
        return BitmapFactory.decodeResource(ctx.getResources(), MusicLibrary.getAlbumRes(mediaId));
    }

    public static List<MediaBrowserCompat.MediaItem> getMediaItems() {
        List<MediaBrowserCompat.MediaItem> result = new ArrayList<>();
        for (MediaMetadataCompat metadata : music.values()) {
            result.add(
                    new MediaBrowserCompat.MediaItem(
                            metadata.getDescription(), MediaBrowserCompat.MediaItem.FLAG_PLAYABLE));
        }
        return result;
    }

    public static String getPreviousSong(String currentMediaId) {
        String prevMediaId = music.lowerKey(currentMediaId);
        if (prevMediaId == null) {
            prevMediaId = music.firstKey();
        }
        return prevMediaId;
    }

    public static String getNextSong(String currentMediaId) {
        String nextMediaId = music.higherKey(currentMediaId);
        if (nextMediaId == null) {
            nextMediaId = music.firstKey();
        }
        return nextMediaId;
    }

    public static MediaMetadataCompat getMetadata(Context ctx, String mediaId) {
        MediaMetadataCompat metadataWithoutBitmap = music.get(mediaId);
        Bitmap albumArt = getAlbumBitmap(ctx, mediaId);

        // Since MediaMetadataCompat is immutable, we need to create a copy to set the album art.
        // We don't set it initially on all items so that they don't take unnecessary memory.
        MediaMetadataCompat.Builder builder = new MediaMetadataCompat.Builder();
        for (String key :
                new String[] {
                    MediaMetadataCompat.METADATA_KEY_MEDIA_ID,
                    MediaMetadataCompat.METADATA_KEY_ALBUM,
                    MediaMetadataCompat.METADATA_KEY_ARTIST,
                    MediaMetadataCompat.METADATA_KEY_GENRE,
                    MediaMetadataCompat.METADATA_KEY_TITLE
                }) {
            builder.putString(key, metadataWithoutBitmap.getString(key));
        }
        builder.putLong(MediaMetadataCompat.METADATA_KEY_DURATION, metadataWithoutBitmap.getLong(MediaMetadataCompat.METADATA_KEY_DURATION));
        builder.putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, albumArt);
        return builder.build();
    }

    private static void createMediaMetadataCompat(
            String mediaId,
            String title,
            String artist,
            String album,
            String genre,
            long duration,
            int musicResId,
            int albumArtResId,
            String albumArtResName) {
        music.put(mediaId, new MediaMetadataCompat.Builder()
                        .putString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID, mediaId)
                        .putString(MediaMetadataCompat.METADATA_KEY_ALBUM, album)
                        .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, artist)
                        .putLong(MediaMetadataCompat.METADATA_KEY_DURATION, duration * 1000)
                        .putString(MediaMetadataCompat.METADATA_KEY_GENRE, genre)
                        .putString(MediaMetadataCompat.METADATA_KEY_ALBUM_ART_URI, getAlbumArtUri(albumArtResName))
                        .putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_ICON_URI, getAlbumArtUri(albumArtResName))
                        .putString(MediaMetadataCompat.METADATA_KEY_TITLE, title)
                        .build());
        albumRes.put(mediaId, albumArtResId);
        musicRes.put(mediaId, musicResId);
    }
}
