package com.adarsh.bhaktamber.activity;

import android.os.Bundle;
import android.widget.TextView;

import com.adarsh.bhaktamber.R;
import com.adarsh.bhaktamber.base.BaseActivity;

public class RachnaActivity extends BaseActivity {
    TextView tvMahima;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer_item);
        setToolbar(getResources().getString(R.string.rachna));

        tvMahima=findViewById(R.id.tv_text);
        tvMahima.setText(getResources().getString(R.string.history));
    }
}
