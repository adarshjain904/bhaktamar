package com.adarsh.bhaktamber.activity;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adarsh.bhaktamber.R;
import com.adarsh.bhaktamber.model.TitleModel;

public class ShowKavyaFragmnet extends Fragment {
    TitleModel kavya;
    // static method to create the MyImageSlider Fragment containing image
    public  static ShowKavyaFragmnet newInstance(TitleModel kavya)
    {
        ShowKavyaFragmnet slider=new ShowKavyaFragmnet();
        Bundle b=new Bundle();
        b.putSerializable("kavya", kavya);
        slider.setArguments(b);
        return slider;
    }
    // get the image id from fragment in this method although we can also get in onCreateView.
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        kavya= (TitleModel) getArguments().getSerializable("kavya");
    }
    // this method returns the view containing the required which is set while creating instance of fragment
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.item_pager, container, false);
        TextView tvTilte=view.findViewById(R.id.tv_tilte);
        TextView tvKavya=view.findViewById(R.id.tv_kavya);
        tvTilte.setText(kavya.getTitle());
        tvKavya.setText(kavya.getKabi());
        return view;
    }
}