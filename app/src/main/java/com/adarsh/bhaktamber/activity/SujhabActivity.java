package com.adarsh.bhaktamber.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.adarsh.bhaktamber.R;
import com.adarsh.bhaktamber.model.SujhabModel;
import com.adarsh.bhaktamber.base.BaseActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SujhabActivity extends BaseActivity implements View.OnClickListener {
    private FirebaseAuth mAuth;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    String userId, name, mobile = "", message;
    EditText etName, etMobile, etMessage;
    Button btnSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        setToolbar(getResources().getString(R.string.sujab));
        etName = findViewById(R.id.et_name);
        etMobile = findViewById(R.id.et_mobile);
        etMessage = findViewById(R.id.et_message);
        btnSend = findViewById(R.id.btn_send);
        btnSend.setOnClickListener(this);

        mAuth = FirebaseAuth.getInstance();
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference("users");

        mAuth.createUserWithEmailAndPassword("deepdiagamber@gmail.com", "123456")
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            Log.e("userSuccess=-=-", user.getEmail());
                        } else {
                            Log.e("Authentication failed.", "yes");
                        }
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_send:
                if (isValid()) {
                    if (checkNetwork(this)) {
                        createUser();
                    }else{
                        Toast.makeText(this,"कृपया इंटरनेट कनेक्शन की जाँच करें", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    private boolean isValid() {
        name = etName.getText().toString();
        message = etMessage.getText().toString();
        mobile = etMobile.getText().toString();
        boolean isValid = true;
        if (TextUtils.isEmpty(name)) {
            etName.setError("कृपया अपना नाम डाले");
            isValid = false;
        } else if (TextUtils.isEmpty(message)) {
            etMessage.setError("कृपया अपना विचार डाले");
            isValid = false;
        }
        return isValid;
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
    }

    private void createUser() {
        if (TextUtils.isEmpty(userId)) {
            userId = mFirebaseDatabase.push().getKey();
        }

        SujhabModel sujhab = new SujhabModel(name, mobile, message);
        mFirebaseDatabase.child(userId).setValue(sujhab);
        addUserChangeListener();
    }

    private void addUserChangeListener() {
        // User data change listener
        mFirebaseDatabase.child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                SujhabModel sujhab = dataSnapshot.getValue(SujhabModel.class);

                if (sujhab == null) {
                    Log.e("data is null!", "yes");
                    return;
                }
                Toast.makeText(SujhabActivity.this, "धन्यवाद", Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.e("Failed to read user", "" + error.toException());
            }
        });
    }


}
