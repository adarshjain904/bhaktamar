package com.adarsh.bhaktamber.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.adarsh.bhaktamber.R;
import com.adarsh.bhaktamber.base.BaseActivity;

import static com.adarsh.bhaktamber.activity.SplashActivity.setStatusBarGradiant;

public class AppDeveloperActivity extends BaseActivity implements View.OnClickListener {
    TextView tvNumber, tvWhatsapp, tvEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_developer);
        setStatusBarGradiant(this);
        setToolbar("Developer Information");
        tvEmail = findViewById(R.id.tv_email);
        tvNumber = findViewById(R.id.tv_number);
        tvWhatsapp = findViewById(R.id.tv_whatsapp);

        tvEmail.setOnClickListener(this);
        tvNumber.setOnClickListener(this);
        tvWhatsapp.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_email:
                openMail();
                break;
            case R.id.tv_number:
                createCall();
                break;
            case R.id.tv_whatsapp:
                openWhatsApp();
                break;
        }
    }

    private void openMail() {
        String[] TO = {tvEmail.getText().toString()};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
      /*  emailIntent.putExtra(Intent.EXTRA_CC, CC);*/
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Enter message here...");
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            finish();
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }

    private void createCall() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" +tvNumber.getText().toString()));
        startActivity(intent);
    }


    public void openWhatsApp() {
        try {
            Intent sendIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + "" + tvWhatsapp.getText().toString() + "?body=" + ""));
            sendIntent.setPackage("com.whatsapp");
            startActivity(sendIntent);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "it may be you dont have whats app", Toast.LENGTH_LONG).show();

        }
    }


}

