package com.adarsh.bhaktamber.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;


import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ShareCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adarsh.bhaktamber.adapter.CategoryAdapter;
import com.adarsh.bhaktamber.R;
import com.adarsh.bhaktamber.base.BaseActivity;
import com.adarsh.bhaktamber.model.TitleModel;

import java.util.ArrayList;

import static com.adarsh.bhaktamber.activity.SplashActivity.setStatusBarGradiant;

public class HomeActivity extends BaseActivity {
    RecyclerView rvCategory;
    CategoryAdapter categoryAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setToolbar(getResources().getString(R.string.audio));

        rvCategory = findViewById(R.id.rv_category);

        categoryAdapter = new CategoryAdapter(this);
        categoryAdapter.setData(list());
        rvCategory.setLayoutManager(new LinearLayoutManager(this));
        rvCategory.setAdapter(categoryAdapter);
    }



    private ArrayList<TitleModel> list() {
        ArrayList<TitleModel> titleModelList = new ArrayList<>();
        TitleModel titleModel_1 = new TitleModel();
        titleModel_1.setTitle(getResources().getString(R.string.bhaktamber_1));
        titleModel_1.setKabi(getResources().getString(R.string.k_1));

        TitleModel titleModel_2 = new TitleModel();
        titleModel_2.setTitle(getResources().getString(R.string.bhaktamber_2));
        titleModel_2.setKabi(getResources().getString(R.string.k_2));

        TitleModel titleModel_3 = new TitleModel();
        titleModel_3.setTitle(getResources().getString(R.string.bhaktamber_3));
        titleModel_3.setKabi(getResources().getString(R.string.k_3));

        TitleModel titleModel_4 = new TitleModel();
        titleModel_4.setTitle(getResources().getString(R.string.bhaktamber_4));
        titleModel_4.setKabi(getResources().getString(R.string.k_4));

        TitleModel titleModel_5 = new TitleModel();
        titleModel_5.setTitle(getResources().getString(R.string.bhaktamber_5));
        titleModel_5.setKabi(getResources().getString(R.string.k_5));

        TitleModel titleModel_6 = new TitleModel();
        titleModel_6.setTitle(getResources().getString(R.string.bhaktamber_6));
        titleModel_6.setKabi(getResources().getString(R.string.k_6));

        TitleModel titleModel_7 = new TitleModel();
        titleModel_7.setTitle(getResources().getString(R.string.bhaktamber_7));
        titleModel_7.setKabi(getResources().getString(R.string.k_7));

        TitleModel titleModel_8 = new TitleModel();
        titleModel_8.setTitle(getResources().getString(R.string.bhaktamber_8));
        titleModel_8.setKabi(getResources().getString(R.string.k_8));

        TitleModel titleModel_9 = new TitleModel();
        titleModel_9.setTitle(getResources().getString(R.string.bhaktamber_9));
        titleModel_9.setKabi(getResources().getString(R.string.k_9));

        TitleModel titleModel_10 = new TitleModel();
        titleModel_10.setTitle(getResources().getString(R.string.bhaktamber_10));
        titleModel_10.setKabi(getResources().getString(R.string.k_10));

        titleModelList.add(titleModel_1);
        titleModelList.add(titleModel_2);
        titleModelList.add(titleModel_3);
        titleModelList.add(titleModel_4);
        titleModelList.add(titleModel_5);
        titleModelList.add(titleModel_6);
        titleModelList.add(titleModel_7);
        titleModelList.add(titleModel_8);
        titleModelList.add(titleModel_9);
        titleModelList.add(titleModel_10);

        return titleModelList;
    }


}
