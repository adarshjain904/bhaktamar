package com.adarsh.bhaktamber.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adarsh.bhaktamber.R;
import com.adarsh.bhaktamber.model.TitleModel;
import com.adarsh.bhaktamber.adapter.ShowNumberAdapter;

import static com.adarsh.bhaktamber.activity.SplashActivity.setStatusBarGradiant;

public class NumberActivity extends AppCompatActivity implements View.OnClickListener {
    RecyclerView rvCategory;
    ShowNumberAdapter showNumberAdapter;
    TitleModel titleModel;
    TextView tvTilte,tvKbi;
    ImageView ivBack;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_number);
        setStatusBarGradiant(this);
        rvCategory = findViewById(R.id.rv_category);
        tvTilte = findViewById(R.id.tv_title_num);
        tvKbi = findViewById(R.id.tv_kbi);
        ivBack = findViewById(R.id.iv_back);
        ivBack.setOnClickListener(this);


        titleModel = (TitleModel) getIntent().getSerializableExtra("TitleModel");

        tvTilte.setText(getResources().getString(R.string.app_name));
        tvKbi.setText("("+titleModel.getKabi()+")");

        showNumberAdapter = new ShowNumberAdapter(this,titleModel.getKabi(), getIntent().getIntExtra("listNumber", 0));
        rvCategory.setLayoutManager(new GridLayoutManager(this, 3));
        rvCategory.setAdapter(showNumberAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_back:
                finish();
                break;
        }
    }
}
