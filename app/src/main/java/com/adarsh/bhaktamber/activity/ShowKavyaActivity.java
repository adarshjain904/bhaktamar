package com.adarsh.bhaktamber.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import com.adarsh.bhaktamber.model.GetData;
import com.adarsh.bhaktamber.R;
import com.adarsh.bhaktamber.model.TitleModel;
import com.adarsh.bhaktamber.adapter.CustomPagerAdapter;
import com.shuhart.bubblepagerindicator.BubblePageIndicator;

import java.util.ArrayList;
import java.util.List;


import static com.adarsh.bhaktamber.activity.SplashActivity.setStatusBarGradiant;

public class ShowKavyaActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener, View.OnClickListener {
    CustomPagerAdapter customPagerAdapter;
    TextView tvTitleKavya, tvKbi;
    List<Fragment> fragments;
    int pos;
    ImageView ivBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_kavya);
        setStatusBarGradiant(this);
        pos = getIntent().getIntExtra("pos", 0);

        ViewPager viewPager = findViewById(R.id.vp_kavya);
        tvTitleKavya = findViewById(R.id.tv_title_kavya);
        tvKbi = findViewById(R.id.tv_kbi);
        ivBack = findViewById(R.id.iv_back);
        ivBack.setOnClickListener(this);


        int newPos = pos + 1;
        tvTitleKavya.setText(getResources().getString(R.string.kavya) + " " + newPos);
        tvKbi.setText(getIntent().getStringExtra("kabi"));

        BubblePageIndicator indicator = findViewById(R.id.indicator);

        if (getIntent().getIntExtra("listNumber", 0) == 0) {
            fragments = getFragments(GetData.list_1(this));
        } else if (getIntent().getIntExtra("listNumber", 0) == 1) {
            fragments = getFragments(GetData.list_2(this));
        } else if (getIntent().getIntExtra("listNumber", 0) == 2) {
            fragments = getFragments(GetData.list_3(this));
        } else if (getIntent().getIntExtra("listNumber", 0) == 3) {
            fragments = getFragments(GetData.list_4(this));
        } else if (getIntent().getIntExtra("listNumber", 0) == 4) {
            fragments = getFragments(GetData.list_5(this));
        } else if (getIntent().getIntExtra("listNumber", 0) == 5) {
            fragments = getFragments(GetData.list_6(this));
        } else if (getIntent().getIntExtra("listNumber", 0) == 6) {
            fragments = getFragments(GetData.list_7(this));
        } else if (getIntent().getIntExtra("listNumber", 0) == 7) {
            fragments = getFragments(GetData.list_8(this));
        } else if (getIntent().getIntExtra("listNumber", 0) == 8) {
            fragments = getFragments(GetData.list_9(this));
        } else if (getIntent().getIntExtra("listNumber", 0) == 9) {
            fragments = getFragments(GetData.list_10(this));
        } else {
            fragments = getFragments(GetData.list_1(this));
        }


        customPagerAdapter = new CustomPagerAdapter(getSupportFragmentManager(), fragments);

        viewPager.setAdapter(customPagerAdapter);
        indicator.setViewPager(viewPager);
        viewPager.setCurrentItem(pos);
        viewPager.setOnPageChangeListener(this);
    }

    private List<Fragment> getFragments(ArrayList<TitleModel> list) {
        List<Fragment> fList = new ArrayList<Fragment>();
        for (int i = 0; i < list.size(); i++) {
            fList.add(ShowKavyaFragmnet.newInstance(list.get(i)));
        }
        return fList;
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        pos = position + 1;
        tvTitleKavya.setText(getResources().getString(R.string.kavya) + " " + pos);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
        }
    }
}
