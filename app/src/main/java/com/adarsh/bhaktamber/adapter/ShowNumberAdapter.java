package com.adarsh.bhaktamber.adapter;


import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adarsh.bhaktamber.R;
import com.adarsh.bhaktamber.model.TitleModel;
import com.adarsh.bhaktamber.activity.ShowKavyaActivity;

import java.util.ArrayList;


public class ShowNumberAdapter extends RecyclerView.Adapter<ShowNumberAdapter.ViewHolder> {
    Context context;
    ArrayList<TitleModel> data;
    int listNumber;
    String tvKbi;

    public ShowNumberAdapter(Context context, String tvKbi, int listNumber) {
        this.context = context;
        this.listNumber = listNumber;
        this.tvKbi = tvKbi;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_number, viewGroup, false);
        return new ShowNumberAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        int a = i + 1;
        viewHolder.tvTitle.setText(context.getResources().getString(R.string.kavya) + String.valueOf(a));


        viewHolder.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ShowKavyaActivity.class);
                intent.putExtra("pos", i);
                intent.putExtra("kabi", tvKbi);
                intent.putExtra("listNumber", listNumber);
                context.startActivity(intent);
            }
        });

    }

    public void setData(ArrayList<TitleModel> data) {
        this.data = data;
    }

    @Override
    public int getItemCount() {
        return 48;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        LinearLayout llItem;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            llItem = itemView.findViewById(R.id.ll_item);



        }
    }
}

