package com.adarsh.bhaktamber.adapter;


import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adarsh.bhaktamber.R;
import com.adarsh.bhaktamber.model.TitleModel;
import com.adarsh.bhaktamber.activity.NumberActivity;

import java.util.ArrayList;


public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {
    Context context;
    ArrayList<TitleModel> data;

    public CategoryAdapter(Context context) {
        this.context = context;

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list, viewGroup, false);
        return new CategoryAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
       viewHolder.tvTitle.setText(data.get(i).getTitle());
       viewHolder.tvKabi.setText(data.get(i).getKabi());
         viewHolder.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, NumberActivity.class);
                intent.putExtra("listNumber",i);
                intent.putExtra("TitleModel",data.get(i));
                context.startActivity(intent);
            }
        });

    }

    public void setData(ArrayList<TitleModel> data) {
        this.data = data;
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvKabi;
        LinearLayout llItem;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvKabi = itemView.findViewById(R.id.tv_kabi);
            llItem = itemView.findViewById(R.id.ll_item);


        }
    }
}

