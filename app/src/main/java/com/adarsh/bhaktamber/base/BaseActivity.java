package com.adarsh.bhaktamber.base;

import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;

import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
;import com.adarsh.bhaktamber.R;


public class BaseActivity extends AppCompatActivity {
    public TextView toolbar_title;
    public ImageView ivBackBtn;
    public static ImageView ivSetting;
    public static TextView tvNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

    }

    public void setToolbar(String title) {
        toolbar_title = findViewById(R.id.tv_title);
        ivBackBtn = findViewById(R.id.iv_back);
         toolbar_title.setText(title);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public static boolean checkNetwork(Context context) {
        boolean connected;
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            connected = true;
        } else
            connected = false;

        return connected;
    }


    public static int getDuration(Context context,int fileName){
        MediaPlayer mp = MediaPlayer.create(context, fileName);
        return mp.getDuration();
    }
/*
    public void dashboardToolBar(String str) {
        toolbar_title = findViewById(R.id.tv_title);
        ivBackBtn = findViewById(R.id.iv_back_btn);
        ivSetting = (ImageView) findViewById(R.id.iv_setting);

        toolbar_title.setText(str);
        ivBackBtn.setVisibility(View.GONE);
        ivSetting.setVisibility(View.GONE);
    }


    public void setToolbarFragment(String str, int visibility, int image) {
        toolbar_title = findViewById(R.id.tv_title);
        ivBackBtn = findViewById(R.id.iv_back_btn);
        ivSetting = findViewById(R.id.iv_setting);


        toolbar_title.setText(str);
        ivBackBtn.setVisibility(View.GONE);
        if (visibility == View.GONE) {
            ivSetting.setVisibility(View.GONE);
        } else {
            ivSetting.setVisibility(View.VISIBLE);
            ivSetting.setImageResource(image);

        }

    }*/


   /* public void setToolBar(String str, View.OnClickListener onBackListener, View.OnClickListener onEditListener) {
        toolbar_title = findViewById(R.id.tv_title);
        ivBackBtn = findViewById(R.id.iv_back_btn);
        tvNext = findViewById(R.id.tv_next);

        toolbar_title.setText(str);
        tvNext.setVisibility(View.VISIBLE);

        ivBackBtn.setOnClickListener(onBackListener);
        tvNext.setOnClickListener(onEditListener);
    }

    public void setToolBarPhoto(String str, String done, View.OnClickListener onEditListener) {
        toolbar_title = findViewById(R.id.tv_title);
        ivBackBtn = findViewById(R.id.iv_back_btn);
        tvNext = findViewById(R.id.tv_next);

        toolbar_title.setText(str);
        tvNext.setVisibility(View.VISIBLE);
        tvNext.setText(done);

        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tvNext.setOnClickListener(onEditListener);
    }
*/


 /*   public void setToolbar(String str, int editImage, View.OnClickListener backListener, View.OnClickListener EditListener) {
        toolbar_title = ((TextView) findViewById(R.id.tv_toolbar_title));
        ivBackBtn = (ImageView) findViewById(R.id.iv_back_btn);
        ivEditBtn = findViewById(R.id.iv_edit_btn);

        toolbar_title.setText(str);
        ivBackBtn.setOnClickListener(backListener);
        ivEditBtn.setOnClickListener(EditListener);
        if (editImage != 0) {
            ivEditBtn.setVisibility(View.VISIBLE);
            ivEditBtn.setImageResource(editImage);
        } else {
            ivEditBtn.setVisibility(View.VISIBLE);
        }
    }*/


}
