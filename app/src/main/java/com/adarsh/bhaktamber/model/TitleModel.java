package com.adarsh.bhaktamber.model;

import java.io.Serializable;

public class TitleModel implements Serializable {
    String title,kabi;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKabi() {
        return kabi;
    }

    public void setKabi(String kabi) {
        this.kabi = kabi;
    }
}
