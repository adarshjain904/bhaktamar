package com.adarsh.bhaktamber.model;

public class SujhabModel {
    String name,mobile,message;

    public SujhabModel() {

    }

    public SujhabModel(String name, String mobile, String message) {
        this.name = name;
        this.mobile = mobile;
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
